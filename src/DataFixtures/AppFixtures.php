<?php

namespace App\DataFixtures;

use App\Entity\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();

        $prodNumToGenerate = 12;
        for ($i=0; $i<$prodNumToGenerate; $i++) {
            $product = new Product();
            $product->setName($faker->text(32));
            $product->setDescription($faker->sentence(12));
            $product->setPrice($faker->randomFloat(2, 1000, 99999));

            $manager->persist($product);
        }

        $manager->flush();
    }
}
