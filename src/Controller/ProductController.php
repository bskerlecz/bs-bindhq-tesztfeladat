<?php

namespace App\Controller;

use App\Entity\Product;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class ProductController extends AbstractController
{
    /**
     * @Route("/", name="product")
     */
    public function index(): Response
    {
        $products = $this->getDoctrine()
            ->getRepository(Product::class)
            ->findAll();

        return $this->render('product/index.html.twig', [
            'products' => $products
        ]);
    }

    /**
     * @Route("/product/delete/{id}", name="product_delete")
     * @param Request $request
     * @param $id
     * @return Response
     */
    public function delete(Request $request, $id): Response {
        $token = $request->get('token');
        
        if ($request->isXmlHttpRequest() && $this->isCsrfTokenValid('delete-product', $token)) {
            $result = $this->getDoctrine()
                ->getRepository(Product::class)
                ->delete($id);

            if ($result) {
                return $this->json(['success' => true]);
            }
        }

        throw new NotFoundHttpException();
    }

}
