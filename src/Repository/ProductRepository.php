<?php

namespace App\Repository;

use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    public function findAll(): array
    {
        return $this->findBy(['deleted' => Product::ACTIVE]);
    }

    public function delete($id) {
        if ($product = $this->find($id)) {
            $product->setDeleted(Product::DELETED);
            $this->getEntityManager()->persist($product);
            $this->getEntityManager()->flush();
            return true;
        } else {
            return false;
        }
    }

}
