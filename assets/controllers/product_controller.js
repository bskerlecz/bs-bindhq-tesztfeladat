import { Controller } from 'stimulus';

export default class extends Controller {
    initialize() {
        this.deleteBtn = null;
        this.deleteUrl = null;
    }

    removeProduct() {
        this.element.remove();
    }

    delete() {
        let req = new XMLHttpRequest();
        req.open("GET", this.deleteUrl);
        req.setRequestHeader("X-Requested-With", "XMLHttpRequest");
        req.addEventListener('load', () => {
            console.log(req.status, req.statusText);
            if (req.status === 200) {
                let res = JSON.parse(req.responseText);
                if (res.success) {
                    // if the request succeeds then we remove the card from the product list
                    this.removeProduct();
                }
            }
        });
        req.send();
    }

    connect() {
        // connect delete button
        this.deleteBtn = this.element.getElementsByClassName('btn-delete')[0];
        this.deleteUrl = this.deleteBtn.getAttribute('data-url');
        this.deleteBtn.addEventListener('click', () => { this.delete(); });
    }
}
