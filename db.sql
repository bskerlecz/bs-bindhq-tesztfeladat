CREATE TABLE product (
	id int(11) not null auto_increment primary key,
	name varchar(255) not null,
	description TEXT default null,
	price DECIMAL(12,2) not null,
	deleted TINYINT(1) not null default 0
) engine=INNODB;

